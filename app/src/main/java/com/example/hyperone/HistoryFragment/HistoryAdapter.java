package com.example.hyperone.HistoryFragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hyperone.HomeFragment.Home.ItemModel;
import com.example.hyperone.R;

import java.util.ArrayList;
import java.util.Calendar;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    ArrayList<ItemModel> items;
    Context context;
    HistoryAdapter.OnItemViewClick onItemViewClick;


    public HistoryAdapter(ArrayList<ItemModel> items, Context context, HistoryAdapter.OnItemViewClick onItemViewClick) {

        this.items = items;
        this.context = context;
        this.onItemViewClick = onItemViewClick;
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_history,parent,false);
        HistoryAdapter.MyViewHolder myViewHolder = new HistoryAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        bindHolderViews(holder, position);
        setHolderOnClickListeners(holder, position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView animalImage;
        TextView type;
        TextView dateTV;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            animalImage = itemView.findViewById(R.id.itemImg);
            type = itemView.findViewById(R.id.typeTV);
            dateTV = itemView.findViewById(R.id.dateTV);
        }

    }

    public interface OnItemViewClick{
        void onItemViewClick(int position);
    }

    private void bindHolderViews(@NonNull HistoryAdapter.MyViewHolder holder, int position) {
        holder.type.setText(items.get(position).getType());
        holder.dateTV.setText(items.get(position).getLastOpen().toString());
        holder.animalImage.setImageResource(items.get(position).getImage());
    }

    private void setHolderOnClickListeners(@NonNull HistoryAdapter.MyViewHolder holder, int position) {
        holder.itemView.setOnClickListener(v -> {
            onItemViewClick.onItemViewClick(position);
            items.get(position).setLastOpen(Calendar.getInstance().getTime());
            HistoryFragment.addToHistory(items.get(position));
            notifyDataSetChanged();
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, items.size());
            notifyDataSetChanged();
        });

    }



}
