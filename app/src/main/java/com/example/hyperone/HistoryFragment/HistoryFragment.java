package com.example.hyperone.HistoryFragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hyperone.ClickItemWindow.ClickItemWindow;
import com.example.hyperone.HomeFragment.Home.HomeFragment;
import com.example.hyperone.HomeFragment.Home.ItemModel;
import com.example.hyperone.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {
    View view;
    RecyclerView recyclerViewHistory;
    static ArrayList<ItemModel> historyItems= new ArrayList<>();
    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_history, container, false);
        initViews();
        implementRV(historyItems);
        return view;
    }

    private void initViews() {
        recyclerViewHistory = view.findViewById(R.id.recyclerView_history);
    }

    private void implementRV(ArrayList<ItemModel> items) {
        HistoryAdapter historyAdapter = new HistoryAdapter(items, getContext(), new HistoryAdapter.OnItemViewClick() {
            @Override
            public void onItemViewClick(int position) {
                HomeFragment.clickItemWindow =
                        new ClickItemWindow(items.get(position));
                HomeFragment.clickItemWindow.show(getActivity().getSupportFragmentManager(),"");
            }
        });
        recyclerViewHistory.setAdapter(historyAdapter);
        recyclerViewHistory.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,true));


        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();
                        items.remove(position);
                        historyAdapter.notifyDataSetChanged();
                    }
                };

        new ItemTouchHelper(simpleCallback).attachToRecyclerView(recyclerViewHistory);

    }

    public static void addToHistory(ItemModel itemModel){
        if(historyItems.contains(itemModel)){
            removeFromHistory(itemModel.getId());
        }
        historyItems.add(itemModel);
    }

    public static void removeFromHistory(int itemId){
        int index = getItemIndex(itemId);
        if(index == -1){return;}
        historyItems.remove(index);
    }

    private static int getItemIndex(int itemId) {
        for (int i = 0 ; i < historyItems.size();i ++ ){
            if(historyItems.get(i).getId() == itemId){
                return i;
            }
        }
        return -1;
    }
}
