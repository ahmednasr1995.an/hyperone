package com.example.hyperone.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.hyperone.R;

public class splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
}
