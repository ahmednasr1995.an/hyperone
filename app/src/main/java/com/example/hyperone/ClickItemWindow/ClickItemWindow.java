package com.example.hyperone.ClickItemWindow;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.models.SlideModel;
import com.example.hyperone.HomeFragment.Home.HomeFragment;
import com.example.hyperone.HomeFragment.Home.ItemModel;
import com.example.hyperone.HomeFragment.Tabs.CartTab.CartFragment.CartFragment;
import com.example.hyperone.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClickItemWindow extends DialogFragment implements View.OnClickListener {
    View view;
    ItemModel item;
    ImageSlider imageSlider;
    TextView typeTV;
    TextView descriptionTV;
    TextView priceTV;
    TextView totalPriceTV;
    RatingBar ratingBar;
    ImageView addToCart;
    ImageView increment;
    ImageView decrement;
    TextView itemCounter;


    public ClickItemWindow(ItemModel item) {
        this.item = item;

    }

    public ClickItemWindow() {
        // Required empty public constructor
    }


    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view  = inflater.inflate(R.layout.fragment_click_item_window, container, false);
        initViesws();
        List<SlideModel> imagesToSlider = imageListToSlideModel(item.getImages());
        imageSlider.setImageList(imagesToSlider,false);

        typeTV.setText(item.getType());
        descriptionTV.setText(item.getDescription());
        ratingBar.setRating((float) item.getRating());
        priceTV.setText(item.getPrice());
        itemCounter.setText(item.getNumberOfItems()+"");
        totalPriceTV.setText(item.getNumberOfItems()*Integer.parseInt(item.getPrice())+"");
        if(item.isInCart()){
            addToCart.setImageResource(R.drawable.ic_in_shopping_cart_black_24dp);
        }else{
            addToCart.setImageResource(R.drawable.ic_add_shopping_cart_black_24dp);
        }
        addToCart.setOnClickListener(this);
        increment.setOnClickListener(this);
        decrement.setOnClickListener(this);



        return view;
    }

    private List<SlideModel> imageListToSlideModel(ArrayList<Integer> resources) {
        List<SlideModel> imagesSlideModel = new ArrayList<>();
        for (Integer resource:resources){
            imagesSlideModel.add(new SlideModel(resource));
        }
        return imagesSlideModel;
    }

    private void initViesws() {
        imageSlider = view.findViewById(R.id.imageSlider);
        typeTV = view.findViewById(R.id.animalTypeOnClick);
        descriptionTV = view.findViewById(R.id.animal_description_on_click);
        ratingBar = view.findViewById(R.id.rating_bar);
        addToCart = view.findViewById(R.id.add_to_cart_btn);
        priceTV = view.findViewById(R.id.priceTV);
        totalPriceTV = view.findViewById(R.id.totalpriceTV);
        increment = view.findViewById(R.id.plus_img);
        decrement = view.findViewById(R.id.minus_img);
        itemCounter = view.findViewById(R.id.quantityTV);
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_to_cart_btn:
                if(!item.isInCart()){
                    String addedToFavourite = item.getType() + " added to your cart";
                    Toast.makeText(getContext(), addedToFavourite, Toast.LENGTH_SHORT).show();
                    item.setInCart(true);
                    addToCart.setImageResource(R.drawable.ic_in_shopping_cart_black_24dp);
                    HomeFragment.updateTabsFavourite();
                    CartFragment.addToCart(item);
                    break;
                }
                String removedFromFavourite = item.getType() + " removed from your cart";
                Toast.makeText(getContext(), removedFromFavourite, Toast.LENGTH_SHORT).show();
                item.setInCart(false);
                addToCart.setImageResource(R.drawable.ic_add_shopping_cart_black_24dp);
                HomeFragment.updateTabsFavourite();
                CartFragment.removeFromCart(item.getId());

                break;
            case R.id.plus_img:
                itemCounter.setText( Integer.parseInt(itemCounter.getText().toString()) + 1 + "");
                item.setNumberOfItems(item.getNumberOfItems() + 1);
                totalPriceTV.setText(item.getNumberOfItems()*Integer.parseInt(item.getPrice())+"");
                break;
            case R.id.minus_img:
                int count = Integer.parseInt(itemCounter.getText().toString());
                if (count > 1) {
                    itemCounter.setText(count - 1 + "");
                    item.setNumberOfItems(item.getNumberOfItems() - 1);
                    totalPriceTV.setText(item.getNumberOfItems()*Integer.parseInt(item.getPrice())+"");
                }else{
                    Toast.makeText(getContext(), "1 is minimum number of items", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }
}
