package com.example.hyperone.MainActivity;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hyperone.AboutUsFragment.AboutUsFragment;
import com.example.hyperone.Auth.LogIn;
import com.example.hyperone.Auth.SignUp;
import com.example.hyperone.BuildConfig;
import com.example.hyperone.EditProfileActivity.EditProfileActivity;
import com.example.hyperone.HomeFragment.Tabs.CartTab.CartFragment.CartFragment;
import com.example.hyperone.FindUsActivity.MapsActivity;
import com.example.hyperone.HistoryFragment.HistoryFragment;
import com.example.hyperone.HomeFragment.Home.HomeFragment;
import com.example.hyperone.R;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.OnConnectionFailedListener;
import com.squareup.picasso.Picasso;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnConnectionFailedListener, GoogleApiClient.OnConnectionFailedListener {
    DrawerLayout drawerLayout;
    ImageView menuImg;
    TextView title;
    Button home;
    Button favourite;
    Button history;
    Button settings;
    Button editProfile;
    Button changeLanguage;
    Button aboutUs;
    Button findUs;
    Button rateUs;
    Button shareUs;
    Button signOut;
    ConstraintLayout settingsMenu;
    HomeFragment homeFragment;
    CartFragment cartFragment;
    HistoryFragment historyFragment;
    AboutUsFragment aboutUsFragment;
    ImageView userImg;
    TextView usernameNav;
    GoogleApiClient googleApiClient;
    GoogleSignInOptions googleSignInOptions;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initViews();
        initFragments();
        loadFacebookData();

        googleSignInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient =  new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions)
                .build();


    }

    private void loadFacebookData() {
        SharedPreferences sharedPreferences = getSharedPreferences("HyperOne",MODE_PRIVATE);
        if(sharedPreferences.getString("Username",null) != null){
            usernameNav.setText(sharedPreferences.getString("Username",null));
            Picasso.get().load(sharedPreferences.getString("profilePicture",null)).placeholder(R.drawable.img).into(userImg);
        }
    }

    private void initFragments() {
        homeFragment= new HomeFragment();
        cartFragment = new CartFragment();
        historyFragment = new HistoryFragment();
        aboutUsFragment = new AboutUsFragment();

        createFragment(homeFragment,home.getText().toString());
    }

    private void initViews() {
        drawerLayout = findViewById(R.id.drawerLayout);
        menuImg = findViewById(R.id.menuImg);
        title = findViewById(R.id.titleTV);
        home = findViewById(R.id.homeButNav);
        history = findViewById(R.id.historyButNav);
        favourite = findViewById(R.id.favButNav);
        settings = findViewById(R.id.settingsButNav);
        editProfile = findViewById(R.id.editProfileButNav);
        changeLanguage = findViewById(R.id.changeLanguageButNav);
        aboutUs = findViewById(R.id.aboutButNav);
        findUs = findViewById(R.id.findUsButNav);
        rateUs = findViewById(R.id.rateUsButNav);
        shareUs = findViewById(R.id.shareUsButNav);
        signOut = findViewById(R.id.signoutButNav);
        settingsMenu = findViewById(R.id.settingsMenuNav);
        userImg = findViewById(R.id.userImg);
        usernameNav = findViewById(R.id.usernameNav);



        menuImg.setOnClickListener(this);
        title.setOnClickListener(this);
        home.setOnClickListener(this);
        history.setOnClickListener(this);
        favourite.setOnClickListener(this);
        settings.setOnClickListener(this);
        editProfile.setOnClickListener(this);
        changeLanguage.setOnClickListener(this);
        aboutUs.setOnClickListener(this);
        findUs.setOnClickListener(this);
        rateUs.setOnClickListener(this);
        shareUs.setOnClickListener(this);
        signOut.setOnClickListener(this);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.menuImg:
                drawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.titleTV:
                break;
            case R.id.homeButNav:
                createFragment(homeFragment,"Home");
                drawerLayout.closeDrawers();
                break;
            case R.id.favButNav:
                createFragment(historyFragment,"Favourite");
                drawerLayout.closeDrawers();
                break;
            case R.id.historyButNav:
                createFragment(historyFragment,history.getText().toString());
                drawerLayout.closeDrawers();
                break;
            case R.id.settingsButNav:
                    if(settingsMenu.getVisibility() == View.GONE){
                        settings.setBackgroundResource(R.drawable.navbuttonselector);
                        settingsMenu.setVisibility(View.VISIBLE);
                        return;
                    }else{
                        settings.setBackgroundResource(R.drawable.navbuttononselect);
                        settingsMenu.setVisibility(View.GONE);
                        return;
                    }
            case R.id.editProfileButNav:
                startActivity(new Intent(MainActivity.this, SignUp.class));
                drawerLayout.closeDrawers();
                break;
            case R.id.changeLanguageButNav:


                drawerLayout.closeDrawers();
                break;
            case R.id.aboutButNav:
                aboutUsFragment.show(getSupportFragmentManager(),aboutUs.getText().toString());
                drawerLayout.closeDrawers();
                break;

            case R.id.findUsButNav:

                startActivity(new Intent(MainActivity.this, MapsActivity.class));
                drawerLayout.closeDrawers();
                break;

            case R.id.rateUsButNav:
                Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
                }

                drawerLayout.closeDrawers();
                break;

            case R.id.shareUsButNav:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "HyperOne App");
                    String shareMessage= "\nLet me recommend you this AMAZING application introduced by HyperOne\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "Choose one"));
                } catch(Exception e) {
                    //e.toString();
                }

                drawerLayout.closeDrawers();
                break;

            case R.id.signoutButNav:
                drawerLayout.closeDrawers();
                signout();
                break;


        }

    }

    private void signout() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_lock_power_off)
                .setTitle("Sign out")
                .setMessage("Are you sure you want to sign out ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                                if(status.isSuccess()){
                                    facebookLogout();
                                    Intent intent = new Intent(MainActivity.this, LogIn.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(MainActivity.this, "Sign out Failed !", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    void createFragment(Fragment fragment, String title){
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.pageContainer,fragment).commit();
        this.title.setText(title);
    }


    private void handleSignInResult(GoogleSignInResult result){
        if(result.isSuccess()){
            GoogleSignInAccount account = result.getSignInAccount();
            usernameNav.setText(account.getDisplayName());
            Uri uri = account.getPhotoUrl();
            Picasso.get().load(account.getPhotoUrl()).placeholder(R.drawable.img).into(userImg);
        }else{
            startActivity(new Intent(MainActivity.this, LogIn.class));
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> optionalPendingResult = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if(optionalPendingResult.isDone()){
            GoogleSignInResult result = optionalPendingResult.get();
            handleSignInResult(result);
        }
        loadFacebookData();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    private void facebookLogout()
    {
        // Logout from Facebook
        if(AccessToken.getCurrentAccessToken() != null)
            LoginManager.getInstance().logOut();
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawers();
            return;
        }
        signout();
    }
}
