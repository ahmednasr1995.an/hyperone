package com.example.hyperone.HomeFragment.Tabs.CartTab.CartFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hyperone.ClickItemWindow.ClickItemWindow;
import com.example.hyperone.HomeFragment.Home.HomeFragment;
import com.example.hyperone.HomeFragment.Home.ItemModel;
import com.example.hyperone.R;

import java.util.ArrayList;
import java.util.Collections;

import static com.example.hyperone.HomeFragment.Home.HomeFragment.items;


/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment {
    View view;
    RecyclerView recyclerViewCart;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView totalCounter;
    int totalPrice;
    static ArrayList<ItemModel> CartItems = new ArrayList<>();

    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_cart, container, false);
        initViews();
        implementRV(CartItems);
        refreshRecyclerView();
        return view;
    }


    private void initViews() {
        recyclerViewCart = view.findViewById(R.id.recyclerView_Cart);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshCart);
        totalCounter = view.findViewById(R.id.priceTotalTV);
        totalCounter.setText(getTotalPrice()+ "");
    }

    private void refreshRecyclerView() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Toast.makeText(getContext(), "You have "+ CartItems.size() + " items in your cart", Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void implementRV(ArrayList<ItemModel> items) {
        recyclerViewCart.setAdapter(new CartAdapter(totalCounter,items, getContext(), new CartAdapter.OnItemViewClick() {
            @Override
            public void onItemViewClick(int position) {
                HomeFragment.clickItemWindow =
                        new ClickItemWindow(items.get(position));
                HomeFragment.clickItemWindow.show(getActivity().getSupportFragmentManager(),"");
            }

            @Override
            public void onRemoveClick(int position) {
                String removedFromYourCart = items.get(position).getType() + " removed from your cart";
                Toast.makeText(getContext(), removedFromYourCart, Toast.LENGTH_SHORT).show();
                items.get(position).setInCart(false);
                totalCounter.setText(getTotalPrice()+ "");
            }

            @Override
            public void onIncrementClick(int position) {
                totalCounter.setText(getTotalPrice()+ "");
            }

            @Override
            public void onDecrementClick(int position) {
                totalCounter.setText(getTotalPrice()+ "");
            }
        }));
        recyclerViewCart.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
    }

    public static void addToCart(ItemModel itemModel){
        CartItems.add(itemModel);

    }

    public static void removeFromCart(int itemId){
        int index = getItemIndex(itemId);
        if(index == -1){return;}
        CartItems.remove(index);

    }

    private static int getItemIndex(int itemId) {
        for (int i = 0; i < CartItems.size(); i ++ ){
            if(CartItems.get(i).getId() == itemId){
                return i;
            }
        }
        return -1;
    }

    private int getTotalPrice(){
        int totalPrice = 0;
        for(ItemModel item : CartItems){
            totalPrice += Integer.parseInt(item.getPrice())*item.getNumberOfItems();
        }
        return totalPrice;
    }

}
