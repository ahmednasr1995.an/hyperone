package com.example.hyperone.HomeFragment.Home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hyperone.HomeFragment.Tabs.CartTab.CartFragment.CartFragment;
import com.example.hyperone.HistoryFragment.HistoryFragment;
import com.example.hyperone.R;

import java.util.ArrayList;
import java.util.Calendar;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.MyViewHolder> {

    ArrayList<ItemModel> items;
    Context context;
    OnItemViewClick onItemViewClick;


    public ItemsAdapter(ArrayList<ItemModel> items, Context context,OnItemViewClick onItemViewClick) {
        this.items = items;
        this.context = context;
        this.onItemViewClick = onItemViewClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_view,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        bindHolderViews(holder, position);
        setHolderOnClickListeners(holder, position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView itemImage;
        ImageView addToCart;
        TextView type;
        TextView price;
        RatingBar ratingBar;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_image);
            addToCart = itemView.findViewById(R.id.add_to_cart_btn);
            type = itemView.findViewById(R.id.typeTV);
            price = itemView.findViewById(R.id.priceTV);
            ratingBar = itemView.findViewById(R.id.rating_bar);
        }
    }

    public interface OnItemViewClick{
        void onItemViewClick(int position);
        void onFavouriteClick(int position);
    }

    private void bindHolderViews(@NonNull MyViewHolder holder, int position) {
        if(items.get(position).isInCart){
            holder.addToCart.setImageResource(R.drawable.ic_in_shopping_cart_black_24dp);
        }else{
            holder.addToCart.setImageResource(R.drawable.ic_add_shopping_cart_black_24dp);
        }
        holder.type.setText(items.get(position).getType());
        holder.price.setText(items.get(position).getPrice());
        holder.itemImage.setImageResource(items.get(position).getImage());
        holder.ratingBar.setRating((float) items.get(position).getRating());
    }

    private void setHolderOnClickListeners(@NonNull MyViewHolder holder, int position) {
        holder.itemView.setOnClickListener(v -> {
            onItemViewClick.onItemViewClick(position);
            items.get(position).setLastOpen(Calendar.getInstance().getTime());
            HistoryFragment.addToHistory(items.get(position));
        });
        holder.addToCart.setOnClickListener(v -> {
            onItemViewClick.onFavouriteClick(position);
            if(checkNotFavouriteDrawable(v, holder)){
                holder.addToCart.setImageResource(R.drawable.ic_in_shopping_cart_black_24dp);
                HomeFragment.updateTabsFavourite();
                CartFragment.addToCart(items.get(position));
                notifyDataSetChanged();

                return;
            }
            holder.addToCart.setImageResource(R.drawable.ic_add_shopping_cart_black_24dp);
            HomeFragment.updateTabsFavourite();
            CartFragment.removeFromCart(items.get(position).getId());
            notifyItemRemoved(position);

        });
    }

    private boolean checkNotFavouriteDrawable(View v, @NonNull MyViewHolder holder) {
        return holder.addToCart.getDrawable().getConstantState()
                == v.getResources().getDrawable(R.drawable.ic_add_shopping_cart_black_24dp).getConstantState();
    }
}
