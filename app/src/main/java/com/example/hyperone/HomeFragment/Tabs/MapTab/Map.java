package com.example.hyperone.HomeFragment.Tabs.MapTab;

import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.hyperone.ClickItemWindow.ClickItemWindow;
import com.example.hyperone.HomeFragment.Home.HomeFragment;
import com.example.hyperone.HomeFragment.Home.ItemModel;
import com.example.hyperone.HomeFragment.Home.ItemsAdapter;
import com.example.hyperone.R;

import java.util.ArrayList;

import static com.example.hyperone.HomeFragment.Home.ItemModel.POPULAR;

/**
 * A simple {@link Fragment} subclass.
 */
public class Map extends Fragment  {

    View view;
    ConstraintLayout constraintLayout;
    public Map() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_popular, container, false);
        constraintLayout = view.findViewById(R.id.mapLayout);
        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(constraintLayout.getBackground().getConstantState()
                        == v.getResources().getDrawable(R.drawable.map).getConstantState()){
                    constraintLayout.setBackgroundResource(R.drawable.foodpart);
            }else{
                    constraintLayout.setBackgroundResource(R.drawable.map);
                }
        }});
    return view;
    }


}
