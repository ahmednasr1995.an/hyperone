package com.example.hyperone.HomeFragment.Home;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hyperone.HomeFragment.Tabs.CartTab.CartFragment.CartFragment;
import com.example.hyperone.ClickItemWindow.ClickItemWindow;
import com.example.hyperone.HomeFragment.Tabs.ProductsTab.Products;
import com.example.hyperone.HomeFragment.Tabs.ScannerTab.Scanner;
import com.example.hyperone.HomeFragment.Tabs.MapTab.Map;
import com.example.hyperone.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    View view;
    TabLayout tab;
    static ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    public static ClickItemWindow clickItemWindow;
    public static int NUMBER_OF_COLUMNS = 2;

    public static ArrayList<ItemModel> items;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_home, container, false);
        initViews();
        initTabView();

        return view;
    }

    private void initTabView() {
        Products productsFragment = new Products();
        Map mapFragment = new Map();
        Scanner scannerFragment = new Scanner();
        CartFragment cartFragment = new CartFragment();

        viewPagerAdapter = new ViewPagerAdapter(this.getChildFragmentManager());

        viewPagerAdapter.addFragment(productsFragment,"Products");
        viewPagerAdapter.addFragment(mapFragment,"Map");
        viewPagerAdapter.addFragment(scannerFragment,"Scanner");
        viewPagerAdapter.addFragment(cartFragment,"Cart");

        viewPager.setAdapter(viewPagerAdapter);
        tab.setupWithViewPager(viewPager);
    }

    private void initViews() {
        tab = view.findViewById(R.id.tab);
        viewPager = view.findViewById(R.id.viewpager);
        items = createItems();
    }

    private ArrayList<ItemModel> createItems() {
        if(this.items == null ){
            ArrayList<ItemModel> items = new ArrayList<>();
            String burgerDescripyion = getString(R.string.burger_description);
            String shampooDescripyion = getString(R.string.shampoo_description);
            items.add(new ItemModel("Burger",((int) (Math.random()*10) + 15) + "",burgerDescripyion,ItemModel.SPECIAL,R.drawable.burger));
            items.add(new ItemModel("Clear Shampoo",((int) (Math.random()*10) + 15) + "",shampooDescripyion,ItemModel.SPECIAL,R.drawable.clear_shampoo));
            items.add(new ItemModel("Corn Flakes",((int) (Math.random()*10) + 15) + "",burgerDescripyion,ItemModel.SPECIAL,R.drawable.corn_flakes));
            items.add(new ItemModel("Heinz Ketchup",((int) (Math.random()*10) + 15) + "",burgerDescripyion,ItemModel.SPECIAL,R.drawable.heinz_ketchup));
            items.add(new ItemModel("Lipton Tea",((int) (Math.random()*10) + 15) + "",burgerDescripyion,ItemModel.SPECIAL,R.drawable.lipton_tea));
            items.add(new ItemModel("Nescafe",((int) (Math.random()*10) + 15) + "",burgerDescripyion,ItemModel.SPECIAL,R.drawable.nescafe));
            ArrayList<Integer> orange_juice_images = new ArrayList<>();
            orange_juice_images.add(R.drawable.orange_juice1);
            orange_juice_images.add(R.drawable.orange_juice2);
            items.add(new ItemModel("Orange Juice",((int) (Math.random()*10) + 15) + "",burgerDescripyion,ItemModel.SPECIAL,R.drawable.orange_juice1,orange_juice_images));
            items.add(new ItemModel("Pants",((int) (Math.random()*10) + 15) + "",burgerDescripyion,ItemModel.SPECIAL,R.drawable.pants));
            items.add(new ItemModel("Plastic Plate",((int) (Math.random()*10) + 15) + "",burgerDescripyion,ItemModel.SPECIAL,R.drawable.plastic_plate));
            ArrayList<Integer> prinkles_images = new ArrayList<>();
            prinkles_images.add(R.drawable.prinkles1);
            prinkles_images.add(R.drawable.prinkles2);
            items.add(new ItemModel("Prinkles",((int) (Math.random()*10) + 15) + "",burgerDescripyion,ItemModel.SPECIAL,R.drawable.prinkles1,prinkles_images));

            return items;
        }
        return items;
    }
    public static void updateTabsFavourite() {
        viewPager.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}
