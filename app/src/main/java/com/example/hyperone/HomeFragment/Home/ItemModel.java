package com.example.hyperone.HomeFragment.Home;

import android.view.ViewGroup;

import com.example.hyperone.R;

import java.util.ArrayList;
import java.util.Date;

public class ItemModel {
    public static final String POPULAR = "Popular";
    public static final String SPECIAL = "Special";
    public static final String OFFER = "Offer";
    String type;
    String price;
    String description;
    String category;
    int image;
    int categoryImage;
    double rating;
    int numberOfItems = 1;
    ArrayList<Integer> images;
    boolean isInCart = false;
    private static int count = 0;
    int id;
    int itemVisit = 0;
    Date lastOpen;


    public int getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    private ArrayList<Integer> defaultAnimalImages(){
        ArrayList<Integer> defaultImages = new ArrayList<>();
            defaultImages.add(this.image);
        return  defaultImages;
    }

    public ItemModel(String type, String price, String description) {
        count++;
        id = count;
        this.type = type;
        this.price = price;
        this.description = description;
        image = R.drawable.hyper_one_logo;
        images = defaultAnimalImages();
        rating = ((int)(Math.random()*5) + 3)*0.5;
    }

    public ItemModel(String type, String price, String description, String category) {
        count++;
        id = count;
        this.type = type;
        this.price = price;
        this.description = description;
        this.category = category;
        image = R.drawable.hyper_one_logo;
        switch (category){
            case POPULAR:
            default:
                categoryImage = R.drawable.default_category_image;
        }
        images = defaultAnimalImages();
        rating = ((int)(Math.random()*5) + 3)*0.5;
    }



    public ItemModel(String type, String price, String description, String category, int image) {
        count++;
        id = count;
        this.type = type;
        this.price = price;
        this.description = description;
        this.category = category;
        this.image = image;
        switch (category){

            default:
                categoryImage = R.drawable.default_category_image;
        }
        images = defaultAnimalImages();
        rating = ((int)(Math.random()*5) + 3)*0.5;

    }

    public ItemModel(String type, String price, String description, String category, int image, ArrayList<Integer> itemImages) {
        count++;
        id = count;
        this.type = type;
        this.price = price;
        this.description = description;
        this.category = category;
        this.image = image;
        switch (category){

            default:
                categoryImage = R.drawable.default_category_image;
        }
        this.images = itemImages;
        rating = ((int)(Math.random()*5) + 3)*0.5;
    }


    public int getCategoryImage() {
        return categoryImage;
    }

    public String getType() {
        return type;
    }

    public String getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public int getImage() {
        return image;
    }

    public int getId() { return id; }

    public String getCategory() {
        return category;
    }

    public ArrayList<Integer> getImages() {
        return images;
    }

    public int getItemVisit() {
        return itemVisit;
    }

    public Date getLastOpen() {
        return lastOpen;
    }

    public void setLastOpen(Date lastOpen) {
        this.lastOpen = lastOpen;
    }

    public void setItemVisit(int itemVisit) {
        this.itemVisit = itemVisit;
    }

    public void setInCart(boolean inCart) {
        isInCart = inCart;
    }

    public boolean isInCart() {
        return isInCart;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setImages(ArrayList<Integer> images) {
        this.images = images;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setCategoryImage(int categoryImage) {
        this.categoryImage = categoryImage;
    }

    public static int getCount() {
        return count;
    }
}
