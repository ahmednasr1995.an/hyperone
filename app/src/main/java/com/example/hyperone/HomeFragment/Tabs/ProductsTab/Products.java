package com.example.hyperone.HomeFragment.Tabs.ProductsTab;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.hyperone.ClickItemWindow.ClickItemWindow;
import com.example.hyperone.HomeFragment.Home.HomeFragment;
import com.example.hyperone.HomeFragment.Home.ItemsAdapter;
import com.example.hyperone.HomeFragment.Home.ItemModel;
import com.example.hyperone.R;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A simple {@link Fragment} subclass.
 */
public class Products extends Fragment {
    View view;
    RecyclerView recyclerViewAll;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<ItemModel> items;

    public Products() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_product, container, false);
        initViews();
        items = HomeFragment.items;
        implementRV(items);
        refreshRecyclerView();

        return view;
    }



    private void initViews() {
        recyclerViewAll = view.findViewById(R.id.recyclerView_all);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshAll);
    }

    private void refreshRecyclerView() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Collections.shuffle(items);
                implementRV(items);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void implementRV(ArrayList<ItemModel> items) {
        recyclerViewAll.setAdapter(new ItemsAdapter(items, getContext(), new ItemsAdapter.OnItemViewClick() {
            @Override
            public void onItemViewClick(int position) {
                HomeFragment.clickItemWindow =
                        new ClickItemWindow(items.get(position));
                HomeFragment.clickItemWindow.show(getActivity().getSupportFragmentManager(),"");
            }

            @Override
            public void onFavouriteClick(int position) {
                if(!items.get(position).isInCart()){
                    String addedToFavourite = items.get(position).getType() + " added to your cart";
                    Toast.makeText(getContext(), addedToFavourite, Toast.LENGTH_SHORT).show();
                    items.get(position).setInCart(true);
                    return;
                }
                String removedFromFavourite =items.get(position).getType() + " removed from your cart";
                Toast.makeText(getContext(), removedFromFavourite, Toast.LENGTH_SHORT).show();
                items.get(position).setInCart(false);
            }
        }));
        recyclerViewAll.setLayoutManager(new GridLayoutManager(getContext(),HomeFragment.NUMBER_OF_COLUMNS));
    }


}
