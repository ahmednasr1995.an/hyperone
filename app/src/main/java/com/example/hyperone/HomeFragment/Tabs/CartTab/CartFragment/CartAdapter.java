package com.example.hyperone.HomeFragment.Tabs.CartTab.CartFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hyperone.HistoryFragment.HistoryFragment;
import com.example.hyperone.HomeFragment.Home.HomeFragment;
import com.example.hyperone.HomeFragment.Home.ItemModel;
import com.example.hyperone.R;

import java.util.ArrayList;
import java.util.Calendar;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    ArrayList<ItemModel> items;
    Context context;
    CartAdapter.OnItemViewClick onItemViewClick;
    TextView totalCounter;




    public CartAdapter(TextView totalCounter,ArrayList<ItemModel> items, Context context, CartAdapter.OnItemViewClick onItemViewClick) {

        this.items = items;
        this.context = context;
        this.onItemViewClick = onItemViewClick;
        this.totalCounter = totalCounter;
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.cart_item_view,parent,false);
        CartAdapter.MyViewHolder myViewHolder = new CartAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        bindHolderViews(holder, position);
        setHolderOnClickListeners(holder, position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView itemImage;
        ImageView removeItem;
        ImageView incrementItem;
        ImageView decrementItem;
        TextView itemCounter;
        TextView type;
        TextView price;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.itemImg);
            removeItem = itemView.findViewById(R.id.remove_item);
            type = itemView.findViewById(R.id.typeTV);
            price = itemView.findViewById(R.id.priceTV);
            incrementItem = itemView.findViewById(R.id.plus_img);
            decrementItem = itemView.findViewById(R.id.minus_img);
            itemCounter = itemView.findViewById(R.id.items_count);
        }

    }
    public interface OnItemViewClick{

        void onItemViewClick(int position);
        void onRemoveClick(int position);
        void onIncrementClick(int position);
        void onDecrementClick(int position);
    }
    @SuppressLint("SetTextI18n")
    private void bindHolderViews(@NonNull CartAdapter.MyViewHolder holder, int position) {
        holder.type.setText(items.get(position).getType());
        holder.price.setText(items.get(position).getPrice());
        holder.itemImage.setBackgroundResource(items.get(position).getImage());
        holder.itemCounter.setText(items.get(position).getNumberOfItems()+"");
    }

    private void setHolderOnClickListeners(@NonNull CartAdapter.MyViewHolder holder, int position) {
        holder.itemView.setOnClickListener(v -> {
            onItemViewClick.onItemViewClick(position);
            items.get(position).setLastOpen(Calendar.getInstance().getTime());
            HistoryFragment.addToHistory(items.get(position));
        });
        holder.removeItem.setOnClickListener(v -> {
            removeItem(holder, position);
        });
        holder.incrementItem.setOnClickListener(v -> {
            holder.itemCounter.setText( Integer.parseInt(holder.itemCounter.getText().toString()) + 1 + "");
            items.get(position).setNumberOfItems(items.get(position).getNumberOfItems() + 1);
            onItemViewClick.onIncrementClick(position);

         });

        holder.decrementItem.setOnClickListener(v -> {
            int count = Integer.parseInt(holder.itemCounter.getText().toString());
            if (count > 1) {
                holder.itemCounter.setText(count - 1 + "");
                items.get(position).setNumberOfItems(items.get(position).getNumberOfItems() - 1);
            }else{removeItem(holder, position);}
            onItemViewClick.onDecrementClick(position);
        });
    }

    private void removeItem(@NonNull MyViewHolder holder, int position) {
        onItemViewClick.onRemoveClick(position);
        ItemModel item = items.get(position);
        item.setNumberOfItems(0);
        HomeFragment.updateTabsFavourite();
        CartFragment.removeFromCart(items.get(position).getId());
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, items.size());
        notifyDataSetChanged();
        item.setNumberOfItems(1);
    }



}
