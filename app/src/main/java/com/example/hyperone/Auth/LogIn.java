package com.example.hyperone.Auth;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ebanx.swipebtn.OnActiveListener;
import com.ebanx.swipebtn.SwipeButton;
import com.example.hyperone.MainActivity.MainActivity;
import com.example.hyperone.R;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LogIn extends AppCompatActivity  implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{

    TextView signUp;
    Button signIn;
    SignInButton signInButton;
    LoginButton loginButtonfacebook;
    SwipeButton googleSwipeButton;
    SwipeButton facebookSwipeButton;
    private GoogleApiClient googleApiClient;
    private static final int SIGN_IN = 1;
    CallbackManager callbackManager;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        initViews();
        initGoogleSignIn();
        initFacebookSignIn();
    }

    private void initFacebookSignIn() {
        callbackManager = CallbackManager.Factory.create();
        loginButtonfacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    private void initViews() {
        signUp = findViewById(R.id.signUpTV);
        signIn = findViewById(R.id.signIn);
        signInButton = findViewById(R.id.signInGoogle);
        loginButtonfacebook = findViewById(R.id.loginbuttonfacebook);
        googleSwipeButton = findViewById(R.id.GoogleSwipeButtton);
        facebookSwipeButton = findViewById(R.id.facebookSwipeButtton);

        signIn.setOnClickListener(this);
        signUp.setOnClickListener(this);
        signInButton.setOnClickListener(this);
        loginButtonfacebook.setReadPermissions(Arrays.asList("email","public_profile"));
        googleSwipeButton.setOnActiveListener(new OnActiveListener() {
            @Override
            public void onActive() {
                SharedPreferences sharedPreferences = getSharedPreferences("PetsStore",MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("profilePicture",null);
                editor.putString("Username",null);
                editor.putString("email",null);
                editor.commit();

                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent,SIGN_IN);

            }
        });

        facebookSwipeButton.setOnActiveListener(new OnActiveListener() {
            @Override
            public void onActive() {
                    loginButtonfacebook.performClick();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpTV:
                startActivity(new Intent(LogIn.this,SignUp.class));
                break;

            case R.id.signIn:
                startActivity(new Intent(LogIn.this, MainActivity.class));
                finish();
                break;

        }
    }

    private void initGoogleSignIn() {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient =  new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions)
                .build();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(result.isSuccess()){
                startActivity(new Intent(LogIn.this, MainActivity.class));
                finish();
            } else {
                Toast.makeText(this, "Login Failed!", Toast.LENGTH_SHORT).show();
            }
        }
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if(currentAccessToken != null){
                loadUserProfile(currentAccessToken);
                startActivity(new Intent(LogIn.this,MainActivity.class));
                finish();
            }
        }
    };
    private void loadUserProfile(final AccessToken accessToken){

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                try {
                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");
                    String email = object.getString("email");
                    String id = object.getString("id");


                    String facebookImgUrL = "https://graph.facebook.com/" + id + "/picture?return_ssl_resources=1";
                    SharedPreferences sharedPreferences = getSharedPreferences("HyperOne",MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("profilePicture",facebookImgUrL);
                    editor.putString("Username",first_name +" "+ last_name);
                    editor.putString("email",email);
                    editor.commit();
                    

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields","first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory( Intent.CATEGORY_HOME );
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }
}
