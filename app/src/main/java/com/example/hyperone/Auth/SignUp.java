package com.example.hyperone.Auth;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.hyperone.R;
import com.hbb20.CountryCodePicker;

public class SignUp extends AppCompatActivity implements View.OnClickListener {
    CountryCodePicker ccp;
    Button signUp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ccp = findViewById(R.id.ccp);
        signUp = findViewById(R.id.signUpBut);
        signUp.setOnClickListener(this);
        ccp.setOnClickListener(this);

        }


    public void onCountryPickerClick(final View view) {
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                //Alert.showMessage(RegistrationActivity.this, ccp.getSelectedCountryCodeWithPlus());
                ccp.getSelectedCountryCodeWithPlus();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ccp:

                break;
            case R.id.signUpBut:

                break;
        }
    }
}




